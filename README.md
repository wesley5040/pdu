# PDU
Final project for ECE434 Embedded Linux in Fall 2018 at Rose-Hulman Institute of Technology with Dr. Yoder.  I did this project alone since no else was interested and it's something I wanted to buy.  I didn't focus on making a "good" UI since I wanted to be able to access it over SSH.  Also, be sure to read the [Some Problems](#markdown-header-some-problems) section.  I'll be fixing them hopefully over the next month or so as I have time.

Note for people thinking about making one:  Currently, in the US, the cost of all the parts isn't much less than buying a used PDU on ebay.

## Contents
+ [PDU Background](#markdown-header-background)
+ [Hardware](#markdown-header-hardware)
	+ [Part List](#markdown-header-list-of-parts)
		+ [Store-bought parts](#markdown-header-store-bought-parts)
		+ [Other parts](#markdown-header-other-parts)
	+ [Assembly](#markdown-header-assembly)
		+ [Case prep](#markdown-header-case-prep)
		+ [Relays and current monitor](#markdown-header-relays-and-current-monitor)
		+ [Internal power and controls](#markdown-header-internal-power-and-controls)
			+ [Relay to Bone mapping](#markdown-header-relay-to-bone-mapping)
			+ [Voltage detect circuit](#markdown-header-voltage-detect-circuit)
+ [OS](#markdown-header-os)
+ [Software](#markdown-header-software)
	+ [Relay Controller](#markdown-header-relay-controller)
		+ [Setup](#markdown-header-relay-controller-setup)
		+ [Usage](#markdown-header-relay-controller-usage)
	+ [Power Monitor](#markdown-header-power-monitor)
		+ [Setup](#markdown-header-power-monitor-setup)
		+ [Usage](#markdown-header-power-monitor-usage)
		+ [Calibration](#markdown-header-power-monitor-calibration)
	+ [Example Setup](#markdown-header-example-setup)
+ [Some Problems](#markdown-header-some-problems)
	+ [Relay Board Voltage](#markdown-header-relay-board-voltage)
	+ [ADC Freezing](#markdown-header-adc-freezing)
+ [Other Notes](#markdown-header-other-notes)
	+ [Future Ideas](#markdown-header-future-ideas)
	+ [Extra Pictures](#markdown-header-extra-pictures)


## Background
A power distribution unit designed for servers running on a BeagleBone Green Wired.  This specific model has 18 outlets with 8 swichable sections and was designed to run at 240V in the US (2 120V lines 180 degrees out of phase), so both hot and "neutral" lines are switched.  I also tested at 120V and 240V single phase.  Each of the 8 channels has a current monitor on it for power usage logging.


## Hardware
### List of parts
These are just the parts I used, most of them could easily (and probably should be) be changed to something else.  I'll explain how I'm using all of them later in this doc.
#### Store-bought parts
+ BeagalBone Green Wired:   [https://www.amazon.com/dp/B01AIC5AP4](https://www.amazon.com/dp/B01AIC5AP4)
+ 8GB microSD card:  [https://www.amazon.com/dp/B00BDS426G](https://www.amazon.com/dp/B00BDS426G)
+ Shallow 2U server case: [https://www.newegg.com/Product/Product.aspx?Item=N82E16811192430](https://www.newegg.com/Product/Product.aspx?Item=N82E16811192430)
+ 8 Channel current monitor: [https://store.ncd.io/product/8-channel-on-board-95-accuracy-20-amp-ac-current-monitor-with-i2c-interface/](https://store.ncd.io/product/8-channel-on-board-95-accuracy-20-amp-ac-current-monitor-with-i2c-interface/)
+ 16 relay board: [https://www.ebay.com/itm/321919083301](https://www.ebay.com/itm/321919083301)
+ 2 - 10 pack of C13 sockets: [https://www.amazon.com/dp/B078RLPFF5](https://www.amazon.com/dp/B078RLPFF5)
+ C20 socket: [https://www.amazon.com/dp/B01N4GD4X3](https://www.amazon.com/dp/B01N4GD4X3)
+ 10 pack of C14 to NEMA 5-15R: [https://www.amazon.com/dp/B07CLNNW6N](https://www.amazon.com/dp/B07CLNNW6N)
+ Small rocker switch: [https://www.amazon.com/dp/B01N2U8PK0](https://www.amazon.com/dp/B01N2U8PK0)
+ 100-240VAC to 12VDC power brick: [https://www.amazon.com/dp/B073QTNF9F](https://www.amazon.com/dp/B073QTNF9F)
+ 12-24VDC to 5VDC power brick: [https://www.amazon.com/dp/B00J3MHT1E](https://www.amazon.com/dp/B00J3MHT1E)
+ Some prototype boards and connectors: [https://www.amazon.com/dp/B07CK3RCKS](https://www.amazon.com/dp/B07CK3RCKS)
+ 24 pack of adhesive standoffs: [https://www.amazon.com/dp/B00R4ZT1FY](https://www.amazon.com/dp/B00R4ZT1FY)
+ 30awg wrapping wire:
[https://www.amazon.com/dp/B008AGUAII](https://www.amazon.com/dp/B008AGUAII)
+ 22awg multi-color solid core wire: [https://www.amazon.com/dp/B008L3QJAS](https://www.amazon.com/dp/B008L3QJAS)
+ Male USB type A to male type A cable: [https://www.amazon.com/dp/B00551Q3CS](https://www.amazon.com/dp/B00551Q3CS)
+ Good mounting tape: [https://www.amazon.com/dp/B00FUEN2GK](https://www.amazon.com/dp/B00FUEN2GK)

#### Other parts
+ A bunch of standard 14/2 Romex
+ A little 10/2 Romex
+ 2 - 100A SquareD Homeline ground/neutral bar lugs
+ Cat5e/6 keystone jack
+ Half of a Cat5e cable
+ 4 - random diodes
+ 4 - 1mF 25V capacitors
+ 2 - 100uF 50V capacitors
+ 2 - 1 kOhm 0.25W resistors
+ 330 Ohm 0.25W resistor
+ 220 Ohm 0.25W resistor
+ 2 - 120V to 15V/30V AC transformers (I pulled them out of old power bricks)
+ C19 power cable
+ MicroUSB cable
+ Piece of sheet metal
+ Electrical tape
+ Twist ties
+ Lots of solder

### Assembly
This is more of a documentation of what I did than a guide to do it again.  I used solid core copper wires for everything since I prefer working with it and I already had a ton laying around.

#### Case prep
To be able to mount all of the sockets in the back of the case I decided to make a backplate in SolidWorks and then have it cut out by a water jet.  The SolidWorks and DXF files are in the [hardware](./hardware) directory.  Here are a few images of the finished part:

![backplate-1](images/backplate-1.jpg)

![backplate-2](images/backplate-2.jpg)

![backplate-3](images/backplate-3.jpg)

![backplate-4](images/backplate-4.jpg)

I also had to unscrew all the freestanding stuff in the case, but that was trivial.

#### Relays and current monitor
I mounted the relay board and current monitors in places that looked right using the adhesive standoffs with the good mounting tape stuck to their adhesive.  After this I ran the ground through each of the connectors and soldered them all together.  I tried soldering the wire to the side of the case, but it didn't hold well so much later in the process I used one of the motherboard standoffs to screw it the case.

Next, I started running the hot wires from the power inlet into the relays and from the relays through the current monitors and to the sockets.  I decided to run the supply into the side of the relay which is connected to the output when the relay is not activated, so theoretically when the BeagalBone has to reboot nothing that is attached will get shut off.  To attach the 14AWG wire from the relays to 10AWG wire going to the power inlet I used a SquareD Homeline ground/neutral bar lug. This is something designed to allow large cables to go into the ground/neutral bars of the SquareD Homeline breaker boxes.  After putting all the wires in I tightened it as much as I could, made sure no wires were loose, then wrapped it a few times in electrical tape.  Then I ran all the wires to the back panel (and soldered them):

![relays-1](images/relays-1.jpg)

After that I added all of the neutral/second hot lines in the same way.  I thought it would be a good idea to make it so the two relays for each block were directly across from each other, but it turns out this was a terrible idea.  I'll explain later.  Here's a picture after all the neutrals were run:

![relays-2](images/relays-2.jpg)

And here's a better picture of the back panel after it was done:

![relays-3](images/relays-3.jpg)

#### Internal power and controls
There is a numbered layout of the controls with a description for each below.  I did not label the 100-240VAC to 12VDC or the 12-24VDC to 5VDC converters since I think they are mostly self explanatory aside from the fact I tied the 12V and 5V grounds together to make a single ground for the entire DC part of the system.  I also changed the front panel power switch to a rocker switch.

![power-1](images/power-1.png)

1.  Front/rear panel IO
	+ The full size and micro USB go up to the two front panel USB ports.  The micro USB is not powered so a cable which has a male type A port on both sides can be used for accessing the Bone from a computer.
	+ The ethernet just goes to the back panel so it can be plugged in.  I was originally going to put another RJ45 jack next to the rear panel ethernet for serial, but that ended up not being needed with the Bone's micro USB going to the front panel.
	+ I also removed the power and heartbeat LED's so they could be connected to the case front panel power and HDD LED's respectively.  The reset button on the case is also connected to the +.
2.  Relay control GPIO pins.  Refer to [the relay to Bone mapping table](#markdown-header-relay-to-bone-mapping) for the pin connections.  There are a bunch of empty pins because I was thinking about doing an RGB LED for each socket block, but I ended up not since I didn't have the parts.  My relay board has transistors and octocouplers built in, so I can just run the outputs from the Bone directly to the board.  Note: for some reason I couldn't get the P8 header to work, so I ended up using some on P9 since I didn't have time to figure out why.
3.  Bone +5V power in (pin P9_05), ground (pin P9_01), i2c to the current sensors (pins P9_19 and P9_20), and the analog port for voltage sensing (pin P9_37).
4.  Refer to [the voltage detect circuit](#markdown-header-voltage-detect-circuit) for an explanation on the circuit.
5.  Ground, +5V, i2c SCL, and i2c SDL are the top group of 4 connections and +12V and ground are the bottom 2.
6.  The top to connections are +12V and ground.  The mess underneath it was a result of poor planning.  I thought it would be nicer to have the relays which are directly across from each other go to the same block, but this ended up crossing a bunch of the connections on the 2 row header...
7.  As with 4, refer to [the voltage detect circuit](#markdown-header-voltage-detect-circuit) for an explanation on the circuit.

##### Relay to Bone mapping
| Relays | Bone pin | Socket block |
| ------ | -------- | ------------ |
| 1, 16  | P8_40    | 7            |
| 2, 15  | P8_36    | 6            |
| 3, 14  | P8_32    | 5            |
| 4, 13  | P8_28    | 4            |
| 5, 12  | P8_24    | 3            |
| 6, 11  | P8_20    | 2            |
| 7, 10  | P8_16    | 1            |
| 8, 9   | P8_12    | 0            |
| Ground | P8_02    | All          |

##### Voltage detect circuit
This should output a DC voltage between 0.5 and 1.7V to go to one of the analog pins on the Bone.  I found online that the Bone sinks 2uA on it's analog pins, so it should be negligible compared to the amount of current going through the voltage divider.

![power-2](images/power-2.png)

1.  Connected to the neutral/second hot line
2.  Connected to the switched hot line
3.  Connected to one of the inputs of section 4 from the previous picture
4.  Connected to the other input of section 4 from the previous picture
5.  Those random diodes arranged in a bridge rectifier
6.  4 1mF capacitors
7.  2 1kOhm resistors
8.  330 Ohm resistor
9.  220 Ohm resistor
10. 100 Ohm resistor
11. Connected to Bone analog pin
12. Connected to ground
13. 2 100uF capacitors (added later)

Here is a screenshot of the scope output from the inputs and output of the rectifier while connected to two phase 240V.  Probes 1 and 2 are grounded together and attached to 3 and 4 from the previous diagram.  Probe 3 is on 11 from the diagram and the probe's ground to 12.  If I zoomed in on the waveform like I should have for the DC, you would be able to see some ripple, which is why I added the extra 2 capacitors later.

![power-3](images/power-3.png)


## OS
I decided to use the Debian 9.5 IoT image since that's what I used in the class and didn't feel like changing anything (even though I much prefer CentOS or Fedora).  Here is the image I used: [https://rcn-ee.com/rootfs/bb.org/testing/2018-10-28/stretch-iot/bone-debian-9.5-iot-armhf-2018-10-28-4gb.img.xz](https://rcn-ee.com/rootfs/bb.org/testing/2018-10-28/stretch-iot/bone-debian-9.5-iot-armhf-2018-10-28-4gb.img.xz)  Then I used Etcher to flash the microSD card.

So it was easier to test from a fresh system, I decided to boot from the eMMC, and just use the SD card as something to image the eMMC from.  To do this, just uncomment this line:
```
cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh
```
of `/boot/uEnv.txt` after you do your normal system setup stuff like running updates and installing a few packages.  Make sure after it copies the files to remove the card or change the file otherwise it will do the copy process again on the next boot up.  Since I don't see anyone that isn't familiar with the process needing this PDU, I'm not going to go over it.  The default username and password are `debian` and `temppwd` though.

## Software
### Relay Controller
For some reason, I thought controlling these through a kernel module was a good idea... It took a while to get working, but it does work and does everything it's supposed to.

#### Relay Controller Setup
Make sure you have the kernel headers installed, then go to the [`src/relay-control`](src/relay-control) directory and run `make`.  To load/unload it you can use `make load` and `make uload` after building it.

#### Relay Controller Usage
Run the `cli` program which was compiled with the `make` for the kernel module.  How to use it is printed when you run it.  It just reads and writes to/from `/dev/relay-controller`.


### Power Monitor
This was written in Python since it's much faster to write and I didn't really have the time to do it in C.  It is Python 2 and not 3 because I used the `pause` package from pip so it would be easier to get it to run at the correct times and I did not see a version for Python 3.

#### Power Monitor Setup
Just use Python 2 to run [`src/power/power_logger.py`](src/power/power_logger.py) to start the logger.  Make sure you have `pause` installed.

#### Power Monitor Usage
Log files are in CSV format in `/pdu/log` by default, and averages are recorded for every minute starting with an ISO timestamp.  Each day has it's own file.  To view a live readings, just `cat /pdu/log/live`.  For it to be continuous, I use the command `watch -n 1 cat /pdu/log/live`.

#### Power Monitor Calibration
Since I made the circuit which took line voltage and made it less than 1.8V, I also had to calibrate it.  I made a script, [`src/power/calibrate-reader.py`](src/power/calibrate-reader.py) which basically just takes a bunch of readings and prints out each of the values and average.  I connected the PDU to a variac and an expensive multimeter to take all the of readings in the lookup table [`src/power/adc_lut.py`](src/power/adc_lut.py).


### Example Setup
I used `tmux` to put the relay cli and current power usage in the same terminal window (or TTY).  It also allows you to detach from it using `^b d` then re-attach to it later using `tmux attach` across SSH sessions.  Here is an example screenshot:

![ui-2](images/ui-1.png)


## Some Problems
### Relay Board Voltage
For some reason, I didn't notice the fact the relay board is using 5V on the GPIO while the Bone is on 3.3V... Because of this there's some very odd issues with getting the relays to switch, and it'll probably burn out the Bone at some point.  Here's a few possible solutions which were recommended to me, I'll have to look into them to see which is the best option:

+ Change the power supply chip on the relay board to a 3.3V instead of 5V
+ Use a voltage divider on each of the GPIO lines
+ Find something which just changes the 5V to 3.3V

### ADC Freezing
When a bigger or inductive load is switched or unplugged from one of the relays, the ADC ports on the Bone lock up and refuse to read.  I noticed when I unplug an inductive load, all of the relay indicator LEDs would flash quickly.  This is what I found on my scope on one of the GPIO lines:

![adc-1](images/adc-1.png)

One on the ECE Lab Technicians suggested this is from a combination of the relay board being at 5V instead of 3.3V and a lack of filter capacitors.  Basically, the Bone is having issues dealing with the higher voltage to begin with, and because of that, it is unable to deal with this interference, which causes something to fail on the Bone with the ADC ports.  He suggested adding a 100pF capacitor over each of the transistor IC's power supply lines on the relay board as well as a 10uF and 100pF on the 12V supply lines.


## Other Notes
I bought a really cheap SPI OLED screen I was planning on using, but there is almost no documentation available so I ended up not using it.

### Future Ideas
+ Make a [pfSense](https://www.pfsense.org/) dashboard plugin to view data and control relays
+ Add a working display and some buttons to the front
+ Make an ncurses interface to bring everything together so something like tmux isn't needed.

### Extra pictures
![extras-1](images/extras-1.jpg)

![extras-2](images/extras-2.jpg)

![extras-3](images/extras-3.jpg)

![extras-4](images/extras-4.jpg)

![extras-5](images/extras-5.jpg)

![extras-6](images/extras-6.jpg)
