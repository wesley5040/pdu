# Reads the sensors and logs power usage per channel

import smbus
import sys, signal
import pause, datetime

from common  import *

log_path     = "/pdu/log/"
isensor_addr = 0x2A
i2c_com      = [0x6A, 0x1, 0x1, 0xC, 0x0, 0x0, 0xA]
i2c_bus      = smbus.SMBus(2)
adc          = ADC(64)


# Graceful exit stuff
def ctrl_c(signum, frame):
	del adc  # FIXME: Why does __del__ give an exception?
	sys.exit(0)
signal.signal(signal.SIGINT, ctrl_c)


# Run loop once on the second
second_powers = []
second_volts  = []
one_sec = datetime.timedelta(seconds=1)
one_min = datetime.timedelta(minutes=1)
log_time = datetime.datetime.now().replace(second=0, microsecond=0) + one_min

# Some declarations to make the loop faster
while True:
	start_time = datetime.datetime.now().replace(microsecond=0) + one_sec

	# Get current from each sensor
	i2c_bus.write_i2c_block_data(isensor_addr, 0x92, i2c_com)
	time.sleep(0.4)
	data = i2c_bus.read_i2c_block_data(isensor_addr, 0x55, 32)
	channel_data = []
	for i in range(8):
		channel_data.append((65536 * data[i * 3    ] +
		                       256 * data[i * 3 + 1] +
		                             data[i * 3 + 2]) / 1000)

	# Calc power on each channel
	line_voltage = adc.get_avg_line()
	# chan_str = ""
	for i in range(8):
		channel_data[i] *= line_voltage
		# chan_str += str(channel_data[i]) + ", "

	# Log results to list
	second_volts.append(line_voltage)
	second_powers.append(channel_data)

	# Output to raw format file
	# live_raw_str = str(line_voltage) + ", " + str(sum(channel_data)) + ", " + chan_str[:-2]
	# print("RT>  " + live_raw_str)
	# live_raw = open(log_path + "live-raw", "w")
	# live_raw.write(live_raw_str + "\n")
	# live_raw.close()

	# Output nice stuff
	live = open(log_path + "live", "w")
	live.write("|-------------------------------------------------------|\n")
	live.write("|                        Summary                        |\n")
	live.write("|    Line Voltage:  {0:3.1f}V       Total Power:  {1:4.0f}W    |\n".format(line_voltage, sum(channel_data)))
	live.write("|-------------------------------------------------------|\n")
	live.write("|                    Channel Wattages                   |\n")
	live.write("| CH 0 | CH 1 | CH 2 | CH 3 | CH 4 | CH 5 | CH 6 | CH 7 |\n")
	live.write("| {0:4.0f} | {1:4.0f} | {2:4.0f} | {3:4.0f} | {4:4.0f} | {5:4.0f} | {6:4.0f} | {7:4.0f} |\n".format(
	            channel_data[0], channel_data[1], channel_data[2], channel_data[3],
	            channel_data[4], channel_data[5], channel_data[6], channel_data[7]))
	live.write("|-------------------------------------------------------|\n")
	live.close()

	# Check if its time to log
	if start_time > log_time:
		log_time += one_min
		chan_avg = [0, 0, 0, 0, 0, 0, 0, 0]
		for i in range(len(second_powers)):
			for j in range(8):
				chan_avg[j] += second_powers[i][j]
		for i in range(8):
			chan_avg[i] /= len(second_powers)
		chan_avg_str = ""
		for i in range(len(chan_avg)):
			chan_avg_str += str(chan_avg[i]) + ", "
		chan_avg_str = chan_avg_str[:-2]
		log_str = log_time.isoformat() + ", " + \
		          str(sum(second_volts) / len(second_volts)) + ", " + \
		          str(sum(chan_avg)) + ", " + chan_avg_str
		print("LF>  " + log_str)

		log = open(log_path + log_time.replace(hour=0, minute=0).isoformat(), "a")
		log.write(log_str + "\n")
		log.close()
		second_volts  = []
		second_powers = []

	pause.until(start_time)
