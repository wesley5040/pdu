# Common stuff better the logger and calibrator

import os, struct, time
from adc_lut import *


def echo(file_path, string):
	f = open(file_path, 'w')
	f.write(string)
	f.close()


class ADC:
	sys_adc_path = "/sys/bus/iio/devices/iio:device0/"
	adc_dev_path = "/dev/iio:device0"
	adc_dev = None
	buf_len = 0

	def __init__(self, adc_buf_len):
		self.buf_len = adc_buf_len
		echo(self.sys_adc_path + "scan_elements/in_voltage2_en", "1")
		echo(self.sys_adc_path + "buffer/length", str(self.buf_len))
		echo(self.sys_adc_path + "buffer/enable", "1")
		self.adc_dev = os.open(self.adc_dev_path, os.O_RDONLY)

	def __del__(self):
		os.close(self.adc_dev)
		echo(self.sys_adc_path + "buffer/enable", "0")

	def get_line(self, adc_volt):
		# FIXME: Make this a binary search
		line_volt = 0
		for i in range(len(adc_lut)):
			if adc_lut[i][0] > adc_volt:
				# Using point slope form for finding the correct value
				# from the LUT
				# y = ((y2 - y1)/(x2 - x1)) * (x - x1) + y1
				line_volt = ((adc_lut[i][1] - adc_lut[i - 1][1]) /
				             (adc_lut[i][0] - adc_lut[i - 1][0])) * \
				            (adc_volt - adc_lut[i - 1][0]) + \
				            adc_lut[i - 1][1]
				break;
		return line_volt

	def get_avg_raw(self):
		return (sum(struct.unpack("H" * self.buf_len,
		                          os.read(self.adc_dev, self.buf_len * 2)))
		        / self.buf_len) * 1.8 / 4095

	def get_avg_line(self):
		return self.get_line(self.get_avg_raw())
