# Takes ADC averages and prints them for creating the ADC LUT

from common import *

runs = 32
slep = 0.25
adc  = ADC(256)

adc_voltages  = []
lin_voltages = []
for i in range(runs):
	adc_voltages.append(adc.get_avg_raw())
	lin_voltages.append(adc.get_line(adc_voltages[i]))
	print("ADC  Run " + str(i) + ": " + str(adc_voltages[i]))
	print("Line Run " + str(i) + ": " + str(lin_voltages[i]))
	time.sleep(slep)

print()
print("ADC Average:  " + str(sum(adc_voltages) / runs))
print("Line Average: " + str(sum(lin_voltages) / runs))
del adc
