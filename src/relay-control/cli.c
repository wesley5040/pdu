// Outline taken from one Derek Molloy's examples http://www.derekmolloy.ie/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define DEV_PATH "/dev/relay-controller"
#define BUFFER_LENGTH 8

static char echo[BUFFER_LENGTH];

#define GET_STR(i) (com[i] == '0') ? "OFF" : "ON "
#define CHK_COM(i) ((com[i] != '0') && (com[i] != '1') && (com[i] != '2'))

void print_syntax() {
	printf("Use 'g' to get the current relay status\n");
	printf("Use 's XXXXXXXX' to set the relays, where 'X' is either\n");
	printf(" '0', '1', or '2', for ON, OFF, and IGNORE respectively\n\n");
}

int main()
{
	int i, ret, rcf;
	char com[BUFFER_LENGTH * 2];

	printf("Opening %s...\n", DEV_PATH);
	if ((rcf = open(DEV_PATH, O_RDWR)) < 0) {
		perror("Failed");
		return errno;
	}

	print_syntax();
	for (;;) {
		printf("> ");
		scanf("%[^\n]%*c", com);

		if (com[0] == 'g') {
			if (read(rcf, com, BUFFER_LENGTH) < 0) {
				perror("Read failed");
				return errno;
			}
			printf("\n");
			printf("CH0: %s    CH1: %s\n", GET_STR(0), GET_STR(1));
			printf("CH2: %s    CH3: %s\n", GET_STR(2), GET_STR(3));
			printf("CH4: %s    CH5: %s\n", GET_STR(4), GET_STR(5));
			printf("CH6: %s    CH7: %s\n", GET_STR(6), GET_STR(7));
			printf("\n");
		} else if (com[0] == 's') {
			if ((com[1] != ' ') || CHK_COM(2) || CHK_COM(3) ||
			         CHK_COM(4) || CHK_COM(5) || CHK_COM(6) ||
			         CHK_COM(7) || CHK_COM(8) || CHK_COM(9)) {
				printf("\nInvalid syntax\n\n");
				print_syntax();
				continue;
			}

			com[BUFFER_LENGTH + 2] = 0;
			if (write(rcf, com + 2, strlen(com + 2)) < 0) {
				perror("Write failed");
				return errno;
			}
		} else {
			print_syntax();
		}
	}

	return 0;
}
