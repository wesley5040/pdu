/**
 * @file   relay_control.h
 * @author Wesley Van Pelt
 * @date   8 Nov 2018
 * @brief  Kernel module header for controlling the 8 pairs of relays connected
 *         to the Bone.
 */
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/gpio.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/uaccess.h>

#define DEVICE_NAME "relay-controller"
#define CLASS_NAME  "RC"
#define RP_COUNT    8


static int dev_num;
static struct class*  rc_char_class = NULL;
static struct device* rc_char_dev   = NULL;
static unsigned int rp[] = {15,//44,  // P8_12 (GPIO1_12)
                            117,//46,  // P8_16 (GPIO1_14)
                            14,//63,  // P8_20 (GPIO1_31)
                            115,//33,  // P8_24 (GPIO1_1)
                            113,//88,  // P8_28 (GPIO2_24)
                            111,//11,  // P8_32 (GPIO0_11)
                            112,//80,  // P8_36 (GPIO2_16)
                            110};//77}; // P8_40 (GPIO2_13)
// TODO? add relay board power to rp[]


static int rc_open(   struct inode *inode, struct file *f);
static int rc_release(struct inode *inode, struct file *f);
static ssize_t rc_read( struct file *f,       char *s, size_t l, loff_t *off);
static ssize_t rc_write(struct file *f, const char *s, size_t l, loff_t *off);
static int  __init rc_init(void);
static void __exit rc_exit(void);


static const struct file_operations rc_fops = {
	.open    = rc_open,
	.read    = rc_read,
	.write   = rc_write,
	.release = rc_release,
};


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Wesley Van Pelt");
MODULE_DESCRIPTION("PDU Relay Controller");
MODULE_VERSION("0.1");

module_init(rc_init);
module_exit(rc_exit);
