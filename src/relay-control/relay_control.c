/**
 * @file   relay_control.c
 * @author Wesley Van Pelt
 * @date   RP_COUNT Nov 201RP_COUNT
 * @brief  Kernel module for controlling the RP_COUNT pairs of relays connected to the
 *         Bone.
 */

#include "relay_control.h"


static int rc_open(struct inode *inode, struct file *f)
{
	return 0;
}


static int rc_release(struct inode *inode, struct file *f)
{
	return 0;
}


static ssize_t rc_read(struct file *f, char *s, size_t l, loff_t *off)
{
	int i;
	char states[RP_COUNT];

	// Get relay pair states
	for (i = 0; i < RP_COUNT; i++)
		states[i] = gpio_get_value(rp[i]) ? '1' : '0';

	// Send back to user
	if (copy_to_user(s, states, RP_COUNT)) {
		printk(KERN_ALERT "RELAY_CONTROLLER: read: copy failed\n");
		return -EFAULT;
	}

	return 0;
}

static ssize_t rc_write(struct file *f, const char *s, size_t l, loff_t *off)
{
	int i;
	char input[RP_COUNT];

	// Prep data
	if (l != RP_COUNT) {
		printk(KERN_INFO "RELAY_CONTROLLER: write: bad input\n");
		return -1;
	}
	if (copy_from_user(input, s, l)) {
		printk(KERN_ALERT "RELAY_CONTROLLER: write: copy failed\n");
		return -EFAULT;
	}

	// Set GPIO
	for (i = 0; i < RP_COUNT; i++) {
		switch (input[i]) {
		case '0':
			gpio_direction_output(rp[i], 0);
			// gpio_set_value(rp[i], 1);  // FIXME
			break;
		case '1':
			gpio_direction_input(rp[i]);
			// gpio_set_value(rp[i], 0);  // FIXME
			break;
		}
	}
	return 0;
}


static int __init rc_init(void)
{
	int i;

	printk(KERN_INFO "RELAY_CONTROLLER: Starting up\n");
	printk(KERN_INFO "RELAY_CONTROLLER: Init GPIO\n");
	for (i = 0; i < RP_COUNT; i++) {
		if (!gpio_is_valid(rp[i])) {
			printk(KERN_ALERT "RELAY_CONTROLLER: Invaild GPIO\n");
			return -ENODEV;
		}
	}
	// Request and set all of the outputs to off so the relays provide power
	for (i = 0; i < RP_COUNT; i++) {
		gpio_request(rp[i], "sysfs");
		gpio_direction_input(rp[i]);
		// gpio_direction_output(rp[i], 0);  // FIXME
		gpio_export(rp[i], 0);
	}
	printk(KERN_INFO "RELAY_CONTROLLER: Init GPIO complete\n");

	// Add /dev stuff
	printk(KERN_INFO "RELAY_CONTROLLER: Init dev stuff\n");
	if ((dev_num = register_chrdev(0, DEVICE_NAME, &rc_fops)) < 0) {
		printk(KERN_ALERT "RELAY_CONTROLLER: Failed to get dev_num\n");
		return dev_num;
	}
	printk(KERN_INFO "RELAY_CONTROLLER: dev_num = %d\n", dev_num);

	rc_char_class = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(rc_char_class)) {
		unregister_chrdev(dev_num, DEVICE_NAME);
		printk(KERN_ALERT "RELAY_CONTROLLER: Failed reg dev class\n");
		return PTR_ERR(rc_char_class);
	}

	rc_char_dev = device_create(rc_char_class, NULL, MKDEV(dev_num, 0),
	                            NULL, DEVICE_NAME);
	if (IS_ERR(rc_char_dev)) {
		class_destroy(rc_char_class);
		unregister_chrdev(dev_num, DEVICE_NAME);
		printk(KERN_ALERT "RELAY_CONTROLLER: Failed create device\n");
		return PTR_ERR(rc_char_dev);
	}

	// TODO? Turn on relay board power

	printk(KERN_INFO "RELAY_CONTROLLER: Init dev stuff complete\n");
	printk(KERN_INFO "RELAY_CONTROLLER: Start up complete\n");
	return 0;
}


static void __exit rc_exit(void)
{
	int i;

	printk(KERN_INFO "RELAY_CONTROLLER: Exiting\n");

	// Remove /dev stuff
	device_destroy(rc_char_class, MKDEV(dev_num, 0));
	class_unregister(rc_char_class);
	class_destroy(rc_char_class);
	unregister_chrdev(dev_num, DEVICE_NAME);

	// TODO? Turn off relay board power

	// unstuff to the GPIO
	for (i = 0; i < RP_COUNT; i++) {
		// gpio_set_value(rp[i], 0);  // FIXME
		gpio_free(rp[i]);
		gpio_unexport(rp[i]);
	}

	printk(KERN_INFO "RELAY_CONTROLLER: Exitted\n");
}
